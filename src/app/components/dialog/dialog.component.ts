import { Component, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalService } from 'src/app/shared/services/modal.service';
import { RegisterComponent } from './templates/register/register.component';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {
  constructor(
    public modalService: ModalService,
  ){
    this.open(RegisterComponent)
  }

  open(componentType: any){
  }
}
