import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  registerForm = this.fb.group({
    firstName:[],
    lastName:[],
    username:[],
    phone:[],
    address:[],
    birthDate:[],
    password:[]
  })
  constructor(
    private fb: FormBuilder
  ){}

  register(){

  }
}
