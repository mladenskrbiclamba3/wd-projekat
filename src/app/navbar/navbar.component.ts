import { Component } from '@angular/core';
import { ModalService } from '../shared/services/modal.service';
import { DialogComponent } from '../components/dialog/dialog.component';
import { RegisterComponent } from '../components/dialog/templates/register/register.component';
import { LoginComponent } from '../components/dialog/templates/login/login.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  isMenuOpen = false;
  isDialogOpen = false;
  constructor(
    private modalService: ModalService,
  ){}
  
  
  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }
  
  register() {
    this.modalService.open(RegisterComponent);

  }

  login(){
    this.modalService.open(LoginComponent);
  }

}
