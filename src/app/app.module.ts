import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { provideDatabase,getDatabase } from '@angular/fire/database';
import { CommonModule } from '@angular/common';
import { AgencyDetailsComponent } from './pages/agency-details/agency-details.component';
import { NgIconsModule } from '@ng-icons/core';
import { heroEnvelope, heroPhone, heroMapPin, heroPencil, heroChevronLeft,heroChevronRight, heroTrash } from '@ng-icons/heroicons/outline';
import { DestinationDetailsComponent } from './pages/destination-details/destination-details.component';
import { EditAgencyComponent } from './pages/edit-agency/edit-agency.component';
import { EditDestinationComponent } from './pages/edit-destination/edit-destination.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogComponent } from './components/dialog/dialog.component';
import { RegisterComponent } from './components/dialog/templates/register/register.component';
import { LoginComponent } from './components/dialog/templates/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ErrorComponent } from './pages/error/error.component';
import { AgenciesAdminComponent } from './pages/agencies-admin/agencies-admin.component';
import { UsersAdminComponent } from './pages/users-admin/users-admin.component';
import { DeleteConfirmationComponent } from './components/dialog/templates/delete-confirmation/delete-confirmation.component';
import { EditUserComponent } from './components/dialog/templates/edit-user/edit-user.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    AgencyDetailsComponent,
    DestinationDetailsComponent,
    EditAgencyComponent,
    EditDestinationComponent,
    DialogComponent,
    RegisterComponent,
    LoginComponent,
    NotFoundComponent,
    ErrorComponent,
    AgenciesAdminComponent,
    UsersAdminComponent,
    DeleteConfirmationComponent,
    EditUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth()),
    provideDatabase(() => getDatabase()),
    NgIconsModule.withIcons({heroEnvelope, heroPhone, heroMapPin, heroPencil, heroChevronLeft,heroChevronRight, heroTrash}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
