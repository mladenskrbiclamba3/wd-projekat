import { Component } from '@angular/core';
import { Database, ref } from '@angular/fire/database';
import { onValue } from '@firebase/database';
import { DeleteConfirmationComponent } from 'src/app/components/dialog/templates/delete-confirmation/delete-confirmation.component';
import { EditUserComponent } from 'src/app/components/dialog/templates/edit-user/edit-user.component';
import { ModalService } from 'src/app/shared/services/modal.service';
import { User } from 'src/app/shared/types/user';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.scss']
})
export class UsersAdminComponent {
  public users: User[]=[];
  private unsubscribe:any;
  constructor(
    private db: Database,
    private modalService: ModalService
    ){}
    ngOnInit() {
      const usersRef = ref(this.db,'korisnici');
      this.unsubscribe = onValue(usersRef, snapshot=>{
        snapshot.forEach(d=>{
          const id = d.ref.key;
          const user = d.val();
          this.users.push({id,...user});
        })
      })
    }
    
    ngOnDestroy() {
      this.unsubscribe();
    }
    
    removeUser() {
      this.modalService.open(DeleteConfirmationComponent);
    }

    editUser(){
      this.modalService.open(EditUserComponent);
    }
  }
