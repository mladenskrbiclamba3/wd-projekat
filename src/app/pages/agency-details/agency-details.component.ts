import { Component } from '@angular/core';
import { Database, onValue, ref } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import { Agency } from 'src/app/shared/types/agency';
import { Destination } from 'src/app/shared/types/destination';

@Component({
  selector: 'app-agency-details',
  templateUrl: './agency-details.component.html',
  styleUrls: ['./agency-details.component.scss'],
  providers: []
})
export class AgencyDetailsComponent {
    private agencyId: string = '';
    public agency: Agency|null = null;
    public destinations: Destination[] = [];
    constructor(
      private db: Database,
      private route: ActivatedRoute,
    ) {}
  
    ngOnInit(): void {
      this.route.params.subscribe(params => {
        this.agencyId = params['id'];
        this.getAgency(this.agencyId);
      });
    }

    getAgency(id: string) {
      const agencyRef = ref(this.db, `agencjie/${id}`);
      onValue(agencyRef, snapshot => {
        const agency = snapshot.val();
        this.agency = agency;
        this.getDestinations(agency.destinacije);
      });
    }

    getDestinations(id: string){
      const destinationsRef = ref(this.db, `destinacije/${id}`);
      onValue(destinationsRef, snapshot => {
        const destinations = snapshot.val();
        Object.entries(destinations).forEach(([key, value]) => {
          this.destinations.push({id: key, ...value as any});
        });
      });
    }
}
