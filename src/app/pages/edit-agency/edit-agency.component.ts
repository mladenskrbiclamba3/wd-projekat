import { Component } from '@angular/core';
import { ref, onValue, Database } from '@angular/fire/database';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Agency } from 'src/app/shared/types/agency';

@Component({
  selector: 'app-edit-agency',
  templateUrl: './edit-agency.component.html',
  styleUrls: ['./edit-agency.component.scss']
})
export class EditAgencyComponent {
  agencyForm = this.fb.group({
    naziv: ['', [Validators.required]],
    adresa: ['', [Validators.required]],
    brojTelefona: ['', [Validators.required]],  
    email: ['', [Validators.required, Validators.email]],
    logo: ['', [Validators.required]],
    godina: ['', [Validators.required]],
  });
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private db: Database
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const agencyId = params['id'];
      if(agencyId){
        this.getAgency(agencyId);
      }
    });
  }

  getAgency(id: string){
    const agencyRef = ref(this.db, `agencjie/${id}`);
      onValue(agencyRef, snapshot => {
        const agency = snapshot.val();
        this.fillForm(agency);
      });
  }

  fillForm(agency: Agency){
    this.agencyForm.patchValue(agency);
  }
  onSubmit() {
    console.log(this.agencyForm.value);
  }
} 
