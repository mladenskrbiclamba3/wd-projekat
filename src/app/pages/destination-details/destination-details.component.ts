import { Component } from '@angular/core';
import { Database, onValue, ref } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import { Destination } from 'src/app/shared/types/destination';

@Component({
  selector: 'app-destination-details',
  templateUrl: './destination-details.component.html',
  styleUrls: ['./destination-details.component.scss']
})
export class DestinationDetailsComponent {
    public destination: Destination | null = null;
    public currentImageIndex: number = 0;
    public destinations: string = '';
    constructor(
      private db: Database,
      private route: ActivatedRoute,
    ) { }


    ngOnInit(): void {
      this.route.params.subscribe(params => {
        const destinationId = params['destinationId'];

        this.route.queryParamMap.pipe().subscribe(queryParams => {
          const destinations = queryParams.get('destinations');
          if (destinations) {
            this.destinations = destinations;
            this.getDestination(destinations,destinationId);
          }
        });
      });
    }

    getDestination(destinations: string,destinationId: string) {
      const destinationRef = ref(this.db, `destinacije/${destinations}/${destinationId}`);
      onValue(destinationRef, snapshot => {
        const destination = snapshot.val();
        this.destination = destination;
      });
    }

    nextImage() {
      if (this.destination && this.currentImageIndex < this.destination.slike.length - 1) {
        this.currentImageIndex++;
      }
    }

    previousImage() {
      if (this.currentImageIndex > 0) {
        this.currentImageIndex--;
      }
    }

}
