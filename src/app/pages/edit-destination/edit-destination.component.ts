import { Component } from '@angular/core';
import { ref, onValue, Database } from '@angular/fire/database';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Destination } from 'src/app/shared/types/destination';

@Component({
  selector: 'app-edit-destination',
  templateUrl: './edit-destination.component.html',
  styleUrls: ['./edit-destination.component.scss']
})
export class EditDestinationComponent {
  destinationForm = this.fb.group({
    naziv: [''],
    cena: [''],
    prevoz: [''],
    tip: [''],
    maxOsoba: [''],
    opis: [''],
  });
  public destinations: string = '';
  public destination: Destination | null = null;
  constructor(
    private fb: FormBuilder,
    private db: Database,
    private route: ActivatedRoute,
    private router: Router
  ) { 
  }


    ngOnInit(): void {
      this.route.params.subscribe(params => {
        const destinationId = params['destinationId'];

        this.route.queryParamMap.pipe().subscribe(queryParams => {
          const destinations = queryParams.get('destinations');
          if (destinations) {
            this.destinations = destinations;
            this.getDestination(destinations,destinationId);
          }
        });
      });
    }

  getDestination(destinations: string,destinationId: string) {
    const destinationRef = ref(this.db, `destinacije/${destinations}/${destinationId}`);
    onValue(destinationRef, snapshot => {
      const destination = snapshot.val();
      this.destination = destination;
      this.fillForm(destination);
    });
  }


  fillForm(destination: Destination) {
    this.destinationForm.patchValue(destination);
  }

  onSubmit() {
    console.log(this.destinationForm.value);
    // this.router.navigate([".."]);
  }
}
