import { Component } from '@angular/core';
import { Database, onValue, ref } from '@angular/fire/database';
import { Agency } from 'src/app/shared/types/agency';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public agencies: Agency[] = [];
  private unsubscribe: any;
  constructor(
    private db: Database
  ) { }

  ngOnInit() {
    const agenciesRef = ref(this.db, 'agencjie');
    this.unsubscribe = onValue(agenciesRef, snapshot => {
      this.agencies = [];
      snapshot.forEach(childSnapshot => {
        const id = childSnapshot.ref.key;
        const agency = childSnapshot.val();
        this.agencies.push({id, ...agency});
      });
    });
  }

  ngOnDestroy() {
    this.unsubscribe();
  }
  

}
