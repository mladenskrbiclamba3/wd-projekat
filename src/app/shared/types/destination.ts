export interface Destination {
    id: string;
    cena: string;
    maxOsoba: string;
    naziv: string;
    opis: string;
    slike: string[];
    tip: string;
    prevoz: string;
}
