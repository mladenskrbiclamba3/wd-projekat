export interface Agency {
    id: string;
    adresa: string;
    email: string;
    brojTelefona: string;
    destinacije: string;
    naziv: string;
    logo: string;
    godina: string;
}
