import { DOCUMENT } from '@angular/common';
import { ComponentFactoryResolver, Inject, Injectable, ViewChild, ViewContainerRef } from '@angular/core';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { RegisterComponent } from 'src/app/components/dialog/templates/register/register.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  public isOpen = false;
  public dialogComponent: any;

  constructor(
  ) { 
  }

  create(){}
  open(component: any){
    this.dialogComponent = component;
    this.isOpen = true;
  }

  close(){
    this.dialogComponent= null;
    this.isOpen = false;
  }
}
