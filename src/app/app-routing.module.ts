import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AgencyDetailsComponent } from './pages/agency-details/agency-details.component';
import { DestinationDetailsComponent } from './pages/destination-details/destination-details.component';
import { EditAgencyComponent } from './pages/edit-agency/edit-agency.component';
import { EditDestinationComponent } from './pages/edit-destination/edit-destination.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ErrorComponent } from './pages/error/error.component';
import { AgenciesAdminComponent } from './pages/agencies-admin/agencies-admin.component';
import { UsersAdminComponent } from './pages/users-admin/users-admin.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'agencies/:id', component: AgencyDetailsComponent },
  { path: 'agencies/:id/edit', component: EditAgencyComponent },
  { path: 'agencies/:id/destinations/:destinationId', component: DestinationDetailsComponent },
  { path: 'agencies/:id/destinations/:destinationId/edit', component: EditDestinationComponent },
  { path: 'admin/agencies', component: AgenciesAdminComponent },
  { path: 'admin/users', component: UsersAdminComponent },
  { path: 'error', component: ErrorComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
